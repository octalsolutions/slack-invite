Version 1.0

A simple web application that allows you to automate sending invitations to your team on Slack.

To get it up and running:

1. npm install 
2. In the file server.js enter your team name and Slack Api Token
3. npm start