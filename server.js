var express = require("express");
var app = express();
var router = express.Router();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/static'));
var path = __dirname + '/';
var request = require('request');


var invite_url = process.env.InviteUrl; // replace process.env.InviteUrl with your team invate url
                                        // (something like 'https://yyyyyy.slack.com/api/users.admin.invite')
var slackToken = process.env.SlackToken; // replace  process.env.SlackToken with your Slack Api Token


app.post('/send', function(req, res) {
        request.post(
            invite_url,
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    console.log(body);
                    var b = JSON.parse(body);
                    if (b.ok){
                        res.sendFile(path + "respond-true.html");
                    }else{
                        res.sendFile(path + "respond-false.html");
                    }
                } else {
                    console.log(error);
                    res.sendFile(path + "respond-false.html");
                }
            }
        ).form({'token':slackToken, 'email':req.body.email});
});



router.get("*",function(req,res){
    res.sendFile(path + "index.html");
});
app.use("/",router);
app.set('port', process.env.PORT || 3000);
app.listen( app.get('port'),function(){
    console.log("Live at " + app.get('port'));
});

